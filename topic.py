from tkinter import *
from tkinter import messagebox
import time
import datetime
import os

root = Tk()

root.state('zoomed')
root.title('--Quizee--')
root.config(bg="#222831")

javapic = PhotoImage(file='images\java.png')
pythonpic = PhotoImage(file='images\python.png')
cpic = PhotoImage(file='images\c.png')
cpppic = PhotoImage(file='images\cpp.png')
htmlpic = PhotoImage(file='images\html.png')
css3pic = PhotoImage(file='images\css3.png')
jspic = PhotoImage(file='images\javascript.png')
sqlpic = PhotoImage(file='images\sql.png')



var=IntVar()
var1=IntVar()
var2=IntVar()
var3=IntVar()
var4=IntVar()
var5=IntVar()
var6=IntVar()
var7=IntVar()
var8=IntVar()
var9=IntVar()
var10=IntVar() 
var11=IntVar()

def ans():
    ansm=()
    #ansm.clear()
    
    ansss=list(ansm)
    print(ansss)
    
    ansss.clear()
    ansm=tuple(ansss)
    print(ansm)

    
    qu1=var.get()
    print(var.get())
    qu2=var1.get()
    qu3=var2.get()
    qu4=var3.get()
    qu5=var4.get()
    qu6=var5.get()
    qu7=var6.get()
    qu8=var7.get()
    qu9=var8.get()
    qu10=var9.get()
    qu11=var10.get()
    qu12=var11.get() 
    ansm=(qu1,qu2,qu3,qu4,qu5,qu6,qu7,qu8,qu9,qu10,qu11,qu12)
    var.set(0)
    var1.set(0)
    var2.set(0)
    var3.set(0)
    var4.set(0)
    var5.set(0)
    var6.set(0)
    var7.set(0)
    var8.set(0)
    var9.set(0)
    var10.set(0)
    var11.set(0)
    
    c=2
    num=[]
    for j in ansm:
        if j==c:
            num.append(j)
    d=len(num)
    
    if(d<1):
        messagebox.showinfo("Marks","0")
    else:
        messagebox.showinfo("Marks",d)
        d=0
        
    del num[:]
mins=StringVar()
sec=StringVar()

def countdown():
    try:
        mins.set("4")
        sec.set("00")

        times = int(mins.get())*60 + int(sec.get())
        while times > -1:
            minute,second = (times // 60 , times % 60)     
            if minute > 60:
                minute = (minute // 60 , minute % 60)      
            sec.set(second)
            mins.set(minute)
       
            root.update()
            time.sleep(1)

            if(time==0):
                sec.set('00')
                mins.set('00')
                ans()
            times -= 1
    except:
        pass

def reset():
    try:
        r_time = int(mins.get()) + int(sec.get())
        while r_time > -1:
            minute,second = (0 ,0)
            sec.set(second-second)
            mins.set(minute-minute)           
            root.update()
    except:
        pass

def main():       

    def Qjs():
        messagebox.showinfo("Quizee","Comming Soon")

    def Qsql():
        messagebox.showinfo("Quizee","Comming Soon")

    def Qjava():
        java.destroy()
        python.destroy()
        c.destroy()
        cpp.destroy()
        html.destroy()
        css.destroy()
        js.destroy()
        sql.destroy()
        greet.destroy()
        usrl.destroy()

        def win1():
            def win2():
                q1.destroy()
                r1.destroy()
                r2.destroy()
                r3.destroy()
                r4.destroy()
                q2.destroy()
                r5.destroy()
                r6.destroy()
                r7.destroy()
                r8.destroy()
                q3.destroy()
                r8.destroy()
                r9.destroy()
                r10.destroy()
                r11.destroy()
                r12.destroy()
                q4.destroy()
                r13.destroy()
                r14.destroy()
                r15.destroy()
                r16.destroy()
                q5.destroy()
                r17.destroy()
                r18.destroy()
                r19.destroy()
                r20.destroy()
                q6.destroy()
                r21.destroy()
                r22.destroy()
                r23.destroy()
                r24.destroy()
                nxt.destroy()

                def chkans7():
                    r25.config(fg="red")
                    r26.config(fg="red")
                    r27.config(fg="red")
                    r28.config(fg="lightgreen")

                def chkans8():
                    r29.config(fg="red")
                    r30.config(fg="red")
                    r31.config(fg="lightgreen")
                    r32.config(fg="red")

                def chkans9():
                    r33.config(fg="red")
                    r34.config(fg="red")
                    r35.config(fg="lightgreen")
                    r36.config(fg="red")

                def chkans10():
                    r37.config(fg="red")
                    r38.config(fg="lightgreen")
                    r39.config(fg="red")
                    r40.config(fg="red")

                def chkans11():
                    r41.config(fg="red")
                    r42.config(fg="lightgreen")
                    r43.config(fg="red")
                    r44.config(fg="red")

                def chkans12():
                    r45.config(fg="red")
                    r46.config(fg="red")
                    r47.config(fg="red")
                    r48.config(fg="lightgreen")
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                q7=Label(root,text="7. Which keyword is used for accessing the features of a package?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q7.place(x=20,y=60)

                r25=Radiobutton(root,text="package",font="Calibri 16",value=21,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r25.place(x=50,y=105)
                r26=Radiobutton(root,text="export",font="Calibri 16",value=22,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r26.place(x=220,y=105)
                r27=Radiobutton(root,text="extends",font="Calibri 16",value=23,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r27.place(x=400,y=105)
                r28=Radiobutton(root,text="import",font="Calibri 16",value=2,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r28.place(x=600,y=105)

                q8=Label(root,text="8. What is meant by the classes and objects that dependents on each other?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q8.place(x=20,y=160)

                r29=Radiobutton(root,text="Cohesion",font="Calibri 16",value=24,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r29.place(x=50,y=205)
                r30=Radiobutton(root,text="Loose Coupling",font="Calibri 16",value=25,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r30.place(x=220,y=205)
                r31=Radiobutton(root,text="Tight Coupling",font="Calibri 16",value=2,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r31.place(x=400,y=205)
                r32=Radiobutton(root,text="None of the above",font="Calibri 16",value=26,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r32.place(x=600,y=205)

                q9=Label(root,text="9. If three threads trying to share a single object at the same time, which condition will arise in this scenario?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q9.place(x=20,y=260)

                r33=Radiobutton(root,text="Time-Lapse",font="Calibri 16",value=27,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r33.place(x=50,y=305)
                r34=Radiobutton(root,text="Critical situation",font="Calibri 16",value=28,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r34.place(x=230,y=305)
                r35=Radiobutton(root,text="Race condition",font="Calibri 16",value=2,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r35.place(x=400,y=305)
                r36=Radiobutton(root,text="Recursion",font="Calibri 16",value=29,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r36.place(x=600,y=305)

                q10=Label(root,text="10. Which environment variable is used to set the java path?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q10.place(x=20,y=360)

                r37=Radiobutton(root,text="MAVEN_HOME",font="Calibri 16",value=30,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r37.place(x=50,y=405)
                r38=Radiobutton(root,text="CLASSPATH",font="Calibri 16",value=2,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r38.place(x=230,y=405)
                r39=Radiobutton(root,text="JAVA",font="Calibri 16",value=31,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r39.place(x=380,y=405)
                r40=Radiobutton(root,text="JAVA_HOME",font="Calibri 16",value=32,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r40.place(x=490,y=405)

                q11=Label(root,text="11. Who invented Java Programming?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q11.place(x=20,y=460)

                r41=Radiobutton(root,text="Guido van Rossum",font="Calibri 16",value=33,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r41.place(x=50,y=505)
                r42=Radiobutton(root,text="James Gosling",font="Calibri 16",value=2,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r42.place(x=250,y=505)
                r43=Radiobutton(root,text="Dennis Ritchie",font="Calibri 16",value=34,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r43.place(x=420,y=505)
                r44=Radiobutton(root,text="Bjarne Stroustrup",font="Calibri 16",value=35,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r44.place(x=580,y=505)

                q12=Label(root,text="12. What is the extension of java code files",font="Calibri 18",bg="#222831",fg="#ffffff")
                q12.place(x=20,y=560)

                r45=Radiobutton(root,text=".js",font="Calibri 16",value=36,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r45.place(x=50,y=605)
                r46=Radiobutton(root,text=".txt",font="Calibri 16",value=37,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r46.place(x=220,y=605)
                r47=Radiobutton(root,text=".class",font="Calibri 16",value=38,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r47.place(x=350,y=605)
                r48=Radiobutton(root,text=".java",font="Calibri 16",value=2,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r48.place(x=490,y=605)


                
                def javaBack():
                    h1.destroy()
                    q7.destroy()
                    r25.destroy()
                    r26.destroy()
                    r27.destroy()
                    r28.destroy()
                    q8.destroy()
                    r29.destroy()
                    r30.destroy()
                    r31.destroy()
                    r32.destroy()
                    q9.destroy()
                    r33.destroy()
                    r34.destroy()
                    r35.destroy()
                    r36.destroy()
                    q10.destroy()
                    r37.destroy()
                    r38.destroy()
                    r39.destroy()
                    r40.destroy()
                    q11.destroy()
                    r41.destroy()
                    r42.destroy()
                    r43.destroy()
                    r44.destroy()
                    q12.destroy()
                    r45.destroy()
                    r46.destroy()
                    r47.destroy()
                    r48.destroy()
                    sbtn.destroy()
                    topic1.destroy()
                    tmin.destroy()
                    tsec.destroy()
                    main()

                def froze():
                    sbtn.config(state=DISABLED)
                    
                sbtn=Button(root,
                            text="Submit",
                            command=lambda:[ans(),froze(),reset()],
                            relief=RAISED,
                            width=25,
                            bd=3,
                            bg="#00fff5",
                            font="Calibri 10",
                            )
                sbtn.place(x=950,y=560)

                topic1=Button(root,
                              text="More quiz",
                              relief=FLAT,
                              width=25,
                              command=lambda:[javaBack(),reset()],
                              bg="#ffffff"
                              )
                topic1.place(x=950,y=610)


            def chkans1():
                r1.config(fg="red")
                r2.config(fg="red")
                r3.config(fg="lightgreen")
                r4.config(fg="red")

            def chkans2():
                r5.config(fg="red")
                r6.config(fg="red")
                r7.config(fg="lightgreen")
                r8.config(fg="red")

            def chkans3():
                r9.config(fg="red")
                r10.config(fg="red")
                r11.config(fg="red")
                r12.config(fg="lightgreen")

            def chkans4():
                r13.config(fg="red")
                r14.config(fg="lightgreen")
                r15.config(fg="red")
                r16.config(fg="red")

            def chkans5():
                r17.config(fg="red")
                r18.config(fg="red")
                r19.config(fg="lightgreen")
                r20.config(fg="red")

            def chkans6():
                r21.config(fg="red")
                r22.config(fg="lightgreen")
                r23.config(fg="red")
                r24.config(fg="red")

                

            h1 = Label(root,text="--Java Quiz--",font="Arail 25 bold",bg="#222831",fg="#ffffff")
            h1.place(x=550,y=0)


            q1=Label(root,text="1. Which of the following is not a Java features?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q1.place(x=20,y=60)

            r1=Radiobutton(root,text="Dynamic",font="Calibri 16",value=1,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r1.place(x=50,y=105)
            r2=Radiobutton(root,text="Architecture Neutral",font="Calibri 16",value=3,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r2.place(x=200,y=105)
            r3=Radiobutton(root,text="Use of pointers",font="Calibri 16",value=2,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r3.place(x=420,y=105)
            r4=Radiobutton(root,text="Object-oriented",font="Calibri 16",value=4,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r4.place(x=620,y=105)

            q2=Label(root,text="2. Which package contains the Random class?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q2.place(x=20,y=160)

            r5=Radiobutton(root,text="java.lang package",font="Calibri 16",value=5,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r5.place(x=50,y=205)
            r6=Radiobutton(root,text="java.awt package",font="Calibri 16",value=6,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r6.place(x=240,y=205)
            r7=Radiobutton(root,text="java.util package",font="Calibri 16",value=2,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r7.place(x=430,y=205)
            r8=Radiobutton(root,text="java.io package",font="Calibri 16",value=8,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r8.place(x=620,y=205)
            
            q3=Label(root,text="3. An interface with no fields or methods is known as a?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q3.place(x=20,y=260)

            r9=Radiobutton(root,text="Runnable Interface",font="Calibri 16",value=9,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r9.place(x=50,y=305)
            r10=Radiobutton(root,text="Abstract Interface",font="Calibri 16",value=10,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r10.place(x=240,y=305)
            r11=Radiobutton(root,text="Abstract Interface",font="Calibri 16",value=11,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r11.place(x=430,y=305)
            r12=Radiobutton(root,text="Marker Interface",font="Calibri 16",value=2,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r12.place(x=630,y=305)

            q4=Label(root,text="4. Which of the following is an immediate subclass of the Panel class?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q4.place(x=20,y=360)

            r13=Radiobutton(root,text="Window class",font="Calibri 16",value=12,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r13.place(x=50,y=405)
            r14=Radiobutton(root,text="Applet class",font="Calibri 16",value=2,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r14.place(x=230,y=405)
            r15=Radiobutton(root,text="Frame class",font="Calibri 16",value=13,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r15.place(x=380,y=405)
            r16=Radiobutton(root,text="Dialog class",font="Calibri 16",value=14,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r16.place(x=530,y=405)

            q5=Label(root,text="5. In which memory a String is stored, when we create a string using new operator?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q5.place(x=20,y=460)

            r17=Radiobutton(root,text="Stack",font="Calibri 16",value=15,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r17.place(x=50,y=505)
            r18=Radiobutton(root,text="String memory",font="Calibri 16",value=16,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r18.place(x=200,y=505)
            r19=Radiobutton(root,text="Heap memory",font="Calibri 16",value=2,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r19.place(x=390,y=505)
            r20=Radiobutton(root,text="Random storage space",font="Calibri 16",value=17,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r20.place(x=550,y=505)

            q6=Label(root,text="6. Which of the following is a reserved keyword in Java?",font="Calibri 18",bg="#222831",fg="#ffffff",activebackground="#222831")
            q6.place(x=20,y=560)

            r21=Radiobutton(root,text="object",font="Calibri 16",value=18,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r21.place(x=50,y=605)
            r22=Radiobutton(root,text="strictfp",font="Calibri 16",value=2,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r22.place(x=200,y=605)
            r23=Radiobutton(root,text="main",font="Calibri 16",value=19,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r23.place(x=350,y=605)
            r24=Radiobutton(root,text="system",font="Calibri 16",value=20,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r24.place(x=480,y=605)


            tmin=Entry(root,
                       textvariable=mins,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Arail 50",
                       relief=FLAT,
                       state=DISABLED)

            tmin.place(x=950,y=50)
            
            tsec=Entry(root,
                       textvariable=sec,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Aail 50",
                       relief=FLAT,
                       state=DISABLED)
            tsec.place(x=1050,y=50)
            
            nxt=Button(root,
                       text="Next",
                       relief=GROOVE,
                       width=12,
                       command=win2,
                       font="Calibri 16 bold",
                       bg="#222831",
                       fg="#ffffff"
                       )
            nxt.place(x=930,y=565)
          
        win1()



    def Qpython():
        java.destroy()
        python.destroy()
        c.destroy()
        cpp.destroy()
        html.destroy()
        css.destroy()
        js.destroy()
        sql.destroy()
        greet.destroy()
        usrl.destroy()
        
        def win1():
            def win2():
                q1.destroy()
                r1.destroy()
                r2.destroy()
                r3.destroy()
                r4.destroy()
                q2.destroy()
                r5.destroy()
                r6.destroy()
                r7.destroy()
                r8.destroy()
                q3.destroy()
                r8.destroy()
                r9.destroy()
                r10.destroy()
                r11.destroy()
                r12.destroy()
                q4.destroy()
                r13.destroy()
                r14.destroy()
                r15.destroy()
                r16.destroy()
                q5.destroy()
                r17.destroy()
                r18.destroy()
                r19.destroy()
                r20.destroy()
                q6.destroy()
                r21.destroy()
                r22.destroy()
                r23.destroy()
                r24.destroy()
                nxt.destroy()


                def chkans7():
                    r25.config(fg="red")
                    r26.config(fg="red")
                    r27.config(fg="red")
                    r28.config(fg="lightgreen")

                def chkans8():
                    r29.config(fg="red")
                    r30.config(fg="red")
                    r31.config(fg="lightgreen")
                    r32.config(fg="red")

                def chkans9():
                    r33.config(fg="red")
                    r34.config(fg="red")
                    r35.config(fg="lightgreen")
                    r36.config(fg="red")

                def chkans10():
                    r37.config(fg="red")
                    r38.config(fg="lightgreen")
                    r39.config(fg="red")
                    r40.config(fg="red")

                def chkans11():
                    r41.config(fg="red")
                    r42.config(fg="lightgreen")
                    r43.config(fg="red")
                    r44.config(fg="red")

                def chkans12():
                    r45.config(fg="red")
                    r46.config(fg="red")
                    r47.config(fg="red")
                    r48.config(fg="lightgreen")
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                q7=Label(root,text="7. All keywords in Python are in?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q7.place(x=20,y=70)

                r25=Radiobutton(root,text="Capitalized",font="Calibri 16",value=21,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r25.place(x=50,y=105)
                r26=Radiobutton(root,text="lower case",font="Calibri 16",value=22,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r26.place(x=220,y=105)
                r27=Radiobutton(root,text="UPPER CASE",font="Calibri 16",value=23,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r27.place(x=400,y=105)
                r28=Radiobutton(root,text="none of the mentioned",font="Calibri 16",value=2,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r28.place(x=600,y=105)

                q8=Label(root,text="8. Which of the following is not a core data type in Python programming?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q8.place(x=20,y=160)

                r29=Radiobutton(root,text="Tuples",font="Calibri 16",value=24,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r29.place(x=50,y=205)
                r30=Radiobutton(root,text="Lists",font="Calibri 16",value=25,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r30.place(x=220,y=205)
                r31=Radiobutton(root,text="Class",font="Calibri 16",value=2,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r31.place(x=400,y=205)
                r32=Radiobutton(root,text="Dictionary",font="Calibri 16",value=26,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r32.place(x=600,y=205)

                q9=Label(root,text="9. To add a new element to a list we use which Python command?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q9.place(x=20,y=260)

                r33=Radiobutton(root,text="list1.addEnd(5)",font="Calibri 16",value=27,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r33.place(x=50,y=305)
                r34=Radiobutton(root,text="list1.addLast(5)",font="Calibri 16",value=28,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r34.place(x=230,y=305)
                r35=Radiobutton(root,text="list1.append(5)",font="Calibri 16",value=2,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r35.place(x=400,y=305)
                r36=Radiobutton(root,text="list1.add(5)",font="Calibri 16",value=29,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r36.place(x=600,y=305)

                q10=Label(root,text="10. Which one of the following is not a keyword in Python language?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q10.place(x=20,y=360)

                r37=Radiobutton(root,text="pass",font="Calibri 16",value=30,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r37.place(x=50,y=405)
                r38=Radiobutton(root,text="eval",font="Calibri 16",value=2,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r38.place(x=230,y=405)
                r39=Radiobutton(root,text="assert",font="Calibri 16",value=31,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r39.place(x=360,y=405)
                r40=Radiobutton(root,text="nonlocal",font="Calibri 16",value=32,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r40.place(x=480,y=405)

                q11=Label(root,text="11. What arithmetic operators cannot be used with strings in Python?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q11.place(x=20,y=460)

                r41=Radiobutton(root,text="*",font="Calibri 16",value=33,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r41.place(x=50,y=505)
                r42=Radiobutton(root,text="-",font="Calibri 16",value=2,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r42.place(x=220,y=505)
                r43=Radiobutton(root,text="+",font="Calibri 16",value=34,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r43.place(x=350,y=505)
                r44=Radiobutton(root,text="All of the mentioned",font="Calibri 16",value=35,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r44.place(x=480,y=505)

                q12=Label(root,text="12. Which one of the following has the highest precedence in the expression?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q12.place(x=20,y=560)

                r45=Radiobutton(root,text="Exponential",font="Calibri 16",value=36,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r45.place(x=50,y=605)
                r46=Radiobutton(root,text="Addition",font="Calibri 16",value=37,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r46.place(x=220,y=605)
                r47=Radiobutton(root,text="Multiplication",font="Calibri 16",value=38,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r47.place(x=350,y=605)
                r48=Radiobutton(root,text="Parentheses",font="Calibri 16",value=2,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r48.place(x=510,y=605)
            
                def home():
                    root.destroy()
                    import intro
                
                def pythonBack():
                    h1.destroy()
                    q7.destroy()
                    r25.destroy()
                    r26.destroy()
                    r27.destroy()
                    r28.destroy()
                    q8.destroy()
                    r29.destroy()
                    r30.destroy()
                    r31.destroy()
                    r32.destroy()
                    q9.destroy()
                    r33.destroy()
                    r34.destroy()
                    r35.destroy()
                    r36.destroy()
                    q10.destroy()
                    r37.destroy()
                    r38.destroy()
                    r39.destroy()
                    r40.destroy()
                    q11.destroy()
                    r41.destroy()
                    r42.destroy()
                    r43.destroy()
                    r44.destroy()
                    q12.destroy()
                    r45.destroy()
                    r46.destroy()
                    r47.destroy()
                    r48.destroy()
                    sbtn.destroy()
                    topic1.destroy()
                    tmin.destroy()
                    tsec.destroy()
                    main()

                def froze():
                    sbtn.config(state=DISABLED)


                sbtn=Button(root,
                            text="Submit",
                            command=lambda:[ans(),froze(),reset()],
                            relief=RAISED,
                            width=25,
                            bd=4,
                            bg="#00fff5",
                            font="Calibri 10",
                            )
                sbtn.place(x=950,y=560)

                topic1=Button(root,
                              text="More quiz",
                              relief=GROOVE,
                              width=25,
                              command=lambda:[pythonBack(),reset()],
                              bg="#ffffff"
                              )
                topic1.place(x=950,y=610)

            
            def chkans1():
                r1.config(fg="lightgreen")
                r2.config(fg="red")
                r3.config(fg="red")
                r4.config(fg="red")

            def chkans2():
                r5.config(fg="red")
                r6.config(fg="red")
                r7.config(fg="lightgreen")
                r8.config(fg="red")

            def chkans3():
                r9.config(fg="red")
                r10.config(fg="red")
                r11.config(fg="red")
                r12.config(fg="lightgreen")
                

            def chkans4():
                r13.config(fg="red")
                r14.config(fg="lightgreen")
                r15.config(fg="red")
                r16.config(fg="red")

            def chkans5():
                r17.config(fg="red")
                r18.config(fg="red")
                r19.config(fg="lightgreen")
                r20.config(fg="red")

            def chkans6():
                r21.config(fg="red")
                r22.config(fg="lightgreen")
                r23.config(fg="red")
                r24.config(fg="red")
                

            h1 = Label(root,text="--Python Quiz--",font="Calibri 25 bold",bg="#222831",fg="#ffffff")
            h1.place(x=550,y=0)

            
            
            q1=Label(root,text="1. Is Python case sensitive when dealing with identifiers?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q1.place(x=20,y=60)

            r1=Radiobutton(root,text="Yes",font="Calibri 16",value=2,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r1.place(x=50,y=105)
            r2=Radiobutton(root,text="no",font="Calibri 16",value=1,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r2.place(x=200,y=105)
            r3=Radiobutton(root,text="machine dependent",font="Calibri 16",value=3,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r3.place(x=350,y=105)
            r4=Radiobutton(root,text="none of the mentioned",font="Calibri 16",value=4,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r4.place(x=600,y=105)

            q2=Label(root,text="2. What is the maximum possible length of an identifier?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q2.place(x=20,y=160)

            r5=Radiobutton(root,text="31 characters",font="Calibri 16",value=5,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r5.place(x=50,y=205)
            r6=Radiobutton(root,text="63 characters",font="Calibri 16",value=6,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r6.place(x=220,y=205)
            r7=Radiobutton(root,text="79 characters",font="Calibri 16",value=2,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r7.place(x=400,y=205)
            r8=Radiobutton(root,text="none of the mentioned",font="Calibri 16",value=8,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r8.place(x=600,y=205)
            
            q3=Label(root,text="3. Which of the following is invalid?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q3.place(x=20,y=260)

            r9=Radiobutton(root,text="_a = 1",font="Calibri 16",value=9,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r9.place(x=50,y=305)
            r10=Radiobutton(root,text="__a = 1",font="Calibri 16",value=10,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r10.place(x=200,y=305)
            r11=Radiobutton(root,text="__str__ = 1",font="Calibri 16",value=11,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r11.place(x=350,y=305)
            r12=Radiobutton(root,text="none of the mentioned",font="Calibri 16",value=2,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r12.place(x=550,y=305)

            q4=Label(root,text="4. Which of the following is an invalid variable?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q4.place(x=20,y=360)

            r13=Radiobutton(root,text="my_string_1",font="Calibri 16",value=12,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r13.place(x=50,y=405)
            r14=Radiobutton(root,text="1st_string",font="Calibri 16",value=2,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r14.place(x=230,y=405)
            r15=Radiobutton(root,text="foo",font="Calibri 16",value=13,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r15.place(x=380,y=405)
            r16=Radiobutton(root,text="_",font="Calibri 16",value=14,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r16.place(x=480,y=405)

            q5=Label(root,text="5. Which of the following is the correct extension of the Python file?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q5.place(x=20,y=460)

            r17=Radiobutton(root,text=".python",font="Calibri 16",value=15,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r17.place(x=50,y=505)
            r18=Radiobutton(root,text=".pl",font="Calibri 16",value=16,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r18.place(x=220,y=505)
            r19=Radiobutton(root,text=".py",font="Calibri 16",value=2,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r19.place(x=350,y=505)
            r20=Radiobutton(root,text=".p",font="Calibri 16",value=17,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r20.place(x=480,y=505)

            q6=Label(root,text="6. Which keyword is used for function in Python language?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q6.place(x=20,y=560)

            r21=Radiobutton(root,text="Function",font="Calibri 16",value=18,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r21.place(x=50,y=605)
            r22=Radiobutton(root,text="Def",font="Calibri 16",value=2,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r22.place(x=220,y=605)
            r23=Radiobutton(root,text="Fun",font="Calibri 16",value=19,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r23.place(x=350,y=605)
            r24=Radiobutton(root,text="Define",font="Calibri 16",value=20,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r24.place(x=480,y=605)
    
            nxt=Button(root,
                       text="Next",
                       relief=GROOVE,
                       width=12,
                       command=win2,
                       font="Calibri 16 bold",
                       bg="#222831",
                       fg="#ffffff"
                       )
            nxt.place(x=930,y=565)

            tmin=Entry(root,
                       textvariable=mins,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Arail 50",
                       relief=FLAT,
                       state=DISABLED)

            tmin.place(x=950,y=50)
            
            tsec=Entry(root,
                       textvariable=sec,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Aail 50",
                       relief=FLAT,
                       state=DISABLED)
            tsec.place(x=1050,y=50)

        win1()

    def Qc():
        java.destroy()
        python.destroy()
        c.destroy()
        cpp.destroy()
        html.destroy()
        css.destroy()
        js.destroy()
        sql.destroy()
        greet.destroy()
        usrl.destroy()
        def win1():
            def win2():
                q1.destroy()
                r1.destroy()
                r2.destroy()
                r3.destroy()
                r4.destroy()
                q2.destroy()
                r5.destroy()
                r6.destroy()
                r7.destroy()
                r8.destroy()
                q3.destroy()
                r8.destroy()
                r9.destroy()
                r10.destroy()
                r11.destroy()
                r12.destroy()
                q4.destroy()
                r13.destroy()
                r14.destroy()
                r15.destroy()
                r16.destroy()
                q5.destroy()
                r17.destroy()
                r18.destroy()
                r19.destroy()
                r20.destroy()
                q6.destroy()
                r21.destroy()
                r22.destroy()
                r23.destroy()
                r24.destroy()
                nxt.destroy()

                def chkans7():
                    r25.config(fg="red")
                    r26.config(fg="red")
                    r27.config(fg="red")
                    r28.config(fg="lightgreen")

                def chkans8():
                    r29.config(fg="red")
                    r30.config(fg="red")
                    r31.config(fg="lightgreen")
                    r32.config(fg="red")

                def chkans9():
                    r33.config(fg="red")
                    r34.config(fg="red")
                    r35.config(fg="green")
                    r36.config(fg="red")

                def chkans10():
                    r37.config(fg="red")
                    r38.config(fg="lightgreen")
                    r39.config(fg="red")
                    r40.config(fg="red")

                def chkans11():
                    r41.config(fg="red")
                    r42.config(fg="red")
                    r43.config(fg="red")
                    r44.config(fg="lightgreen")

                def chkans12():
                    r45.config(fg="red")
                    r46.config(fg="red")
                    r47.config(fg="red")
                    r48.config(fg="lightgreen")
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                q7=Label(root,text="7. What is an example of iteration in C?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q7.place(x=20,y=60)

                r25=Radiobutton(root,text="for",font="Calibri 16",value=21,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r25.place(x=50,y=105)
                r26=Radiobutton(root,text="while",font="Calibri 16",value=22,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r26.place(x=220,y=105)
                r27=Radiobutton(root,text="do-while",font="Calibri 16",value=23,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r27.place(x=400,y=105)
                r28=Radiobutton(root,text="all of the mentioned",font="Calibri 16",value=2,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r28.place(x=600,y=105)

                q8=Label(root,text="8. C is _______ type of programming language.?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q8.place(x=20,y=160)

                r29=Radiobutton(root,text="object Oriented",font="Calibri 16",value=24,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r29.place(x=50,y=205)
                r30=Radiobutton(root,text="Bit level language",font="Calibri 16",value=25,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r30.place(x=220,y=205)
                r31=Radiobutton(root,text="Procedural",font="Calibri 16",value=2,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r31.place(x=400,y=205)
                r32=Radiobutton(root,text="Functional",font="Calibri 16",value=26,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r32.place(x=600,y=205)

                q9=Label(root,text="9. C language was invented in which laboratories?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q9.place(x=20,y=260)

                r33=Radiobutton(root,text="Uniliver Labs",font="Calibri 16",value=27,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r33.place(x=50,y=305)
                r34=Radiobutton(root,text="IBM Labs",font="Calibri 16",value=28,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r34.place(x=230,y=305)
                r35=Radiobutton(root,text="AT&T Bell Labs",font="Calibri 16",value=2,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r35.place(x=400,y=305)
                r36=Radiobutton(root,text="Verizon Labs",font="Calibri 16",value=29,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r36.place(x=600,y=305)

                q10=Label(root,text="10. C language was invented in the year?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q10.place(x=20,y=360)

                r37=Radiobutton(root,text="1999",font="Calibri 16",value=30,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r37.place(x=50,y=405)
                r38=Radiobutton(root,text="1972",font="Calibri 16",value=2,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r38.place(x=230,y=405)
                r39=Radiobutton(root,text="1990",font="Calibri 16",value=31,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r39.place(x=380,y=405)
                r40=Radiobutton(root,text="1978",font="Calibri 16",value=32,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r40.place(x=490,y=405)

                q11=Label(root,text="11. An Identifier can start with?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q11.place(x=20,y=460)

                r41=Radiobutton(root,text="Alphabet",font="Calibri 16",value=33,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r41.place(x=50,y=505)
                r42=Radiobutton(root,text="Underscore ( _ ) sign",font="Calibri 16",value=34,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r42.place(x=200,y=505)
                r43=Radiobutton(root,text="Number",font="Calibri 16",value=35,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r43.place(x=440,y=505)
                r44=Radiobutton(root,text="All of the above",font="Calibri 16",value=2,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r44.place(x=600,y=505)

                q12=Label(root,text="12. Any character that can be typed on a keyboard",font="Calibri 18",bg="#222831",fg="#ffffff")
                q12.place(x=20,y=560)

                r45=Radiobutton(root,text="Float",font="Calibri 16",value=36,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r45.place(x=50,y=605)
                r46=Radiobutton(root,text="Int",font="Calibri 16",value=37,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r46.place(x=220,y=605)
                r47=Radiobutton(root,text="Long",font="Calibri 16",value=38,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r47.place(x=350,y=605)
                r48=Radiobutton(root,text="double",font="Calibri 16",value=2,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r48.place(x=490,y=605)

                def home():
                    root.destroy()
                    import intro
                
                def cBack():
                    h1.destroy()
                    q7.destroy()
                    r25.destroy()
                    r26.destroy()
                    r27.destroy()
                    r28.destroy()
                    q8.destroy()
                    r29.destroy()
                    r30.destroy()
                    r31.destroy()
                    r32.destroy()
                    q9.destroy()
                    r33.destroy()
                    r34.destroy()
                    r35.destroy()
                    r36.destroy()
                    q10.destroy()
                    r37.destroy()
                    r38.destroy()
                    r39.destroy()
                    r40.destroy()
                    q11.destroy()
                    r41.destroy()
                    r42.destroy()
                    r43.destroy()
                    r44.destroy()
                    q12.destroy()
                    r45.destroy()
                    r46.destroy()
                    r47.destroy()
                    r48.destroy()
                    sbtn.destroy()
                    topic1.destroy()
                    tmin.destroy()
                    tsec.destroy()
                    main()

                def froze():
                    sbtn.config(state=DISABLED)

                sbtn=Button(root,
                            text="Submit",
                            command=lambda:[ans(),froze(),reset()],
                            relief=RAISED,
                            width=25,
                            bd=4,
                            bg="#00fff5",
                            font="Calibri 10",
                            )
                sbtn.place(x=950,y=560)



                topic1=Button(root,
                              text="More quiz",
                              relief=GROOVE,
                              width=25,
                              command=lambda:[cBack(),reset()],
                              bg="#ffffff"
                              )
                topic1.place(x=950,y=610)

            def chkans1():
                r1.config(fg="red")
                r2.config(fg="red")
                r3.config(fg="red")
                r4.config(fg="lightgreen")

            def chkans2():
                r5.config(fg="red")
                r6.config(fg="red")
                r7.config(fg="lightgreen")
                r8.config(fg="red")

            def chkans3():
                r9.config(fg="red")
                r10.config(fg="red")
                r11.config(fg="red")
                r12.config(fg="lightgreen")

            def chkans4():
                r13.config(fg="red")
                r14.config(fg="lightgreen")
                r15.config(fg="red")
                r16.config(fg="red")

            def chkans5():
                r17.config(fg="red")
                r18.config(fg="red")
                r19.config(fg="lightgreen")
                r20.config(fg="red")

            def chkans6():
                r21.config(fg="red")
                r22.config(fg="lightgreen")
                r23.config(fg="red")
                r24.config(fg="red")

            h1 = Label(root,text="--C Quiz--",font="Calibri 25 bold",bg="#222831",fg="#ffffff")
            h1.place(x=550,y=0)


            q1=Label(root,text="1. Who is the father of C language?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q1.place(x=20,y=60)

            r1=Radiobutton(root,text="Steve Jobs",font="Calibri 16",value=1,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r1.place(x=50,y=105)
            r2=Radiobutton(root,text="James Gosling",font="Calibri 16",value=3,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r2.place(x=200,y=105)
            r3=Radiobutton(root,text="Rasmus Lerdorf",font="Calibri 16",value=4,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r3.place(x=420,y=105)
            r4=Radiobutton(root,text="Dennis Ritchie",font="Calibri 16",value=2,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r4.place(x=620,y=105)

            q2=Label(root,text="2. All keywords in C are in ______",font="Calibri 18",bg="#222831",fg="#ffffff")
            q2.place(x=20,y=160)

            r5=Radiobutton(root,text="UpperCase letters",font="Calibri 16",value=5,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r5.place(x=50,y=205)
            r6=Radiobutton(root,text="CamelCase letters",font="Calibri 16",value=6,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r6.place(x=240,y=205)
            r7=Radiobutton(root,text="LowerCase letters",font="Calibri 16",value=2,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r7.place(x=430,y=205)
            r8=Radiobutton(root,text="None of the mentioned",font="Calibri 16",value=8,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r8.place(x=620,y=205)
            
            q3=Label(root,text="3. Which is valid C expression?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q3.place(x=20,y=260)

            r9=Radiobutton(root,text="int my_num = 100,000;",font="Calibri 16",value=9,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r9.place(x=50,y=305)
            r10=Radiobutton(root,text="int $my_num = 10000;",font="Calibri 16",value=10,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r10.place(x=280,y=305)
            r11=Radiobutton(root,text="int my num = 1000;",font="Calibri 16",value=11,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r11.place(x=510,y=305)
            r12=Radiobutton(root,text="int my_num = 100000;",font="Calibri 16",value=2,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r12.place(x=710,y=305)

            q4=Label(root,text="4. The C-preprocessors are specified with _________symbol.",font="Calibri 18",bg="#222831",fg="#ffffff")
            q4.place(x=20,y=360)

            r13=Radiobutton(root,text="$",font="Calibri 16",value=12,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r13.place(x=50,y=405)
            r14=Radiobutton(root,text="#",font="Calibri 16",value=2,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r14.place(x=230,y=405)
            r15=Radiobutton(root,text="” ”",font="Calibri 16",value=13,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r15.place(x=380,y=405)
            r16=Radiobutton(root,text="&",font="Calibri 16",value=14,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r16.place(x=530,y=405)

            q5=Label(root,text="5. scanf() is a predefined function in______header file.",font="Calibri 18",bg="#222831",fg="#ffffff")
            q5.place(x=20,y=460)

            r17=Radiobutton(root,text="ctype. h",font="Calibri 16",value=15,variable=var,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r17.place(x=50,y=505)
            r18=Radiobutton(root,text="stdlib. h",font="Calibri 16",value=16,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r18.place(x=200,y=505)
            r19=Radiobutton(root,text="stdio. h",font="Calibri 16",value=2,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r19.place(x=390,y=505)
            r20=Radiobutton(root,text="stdarg. h",font="Calibri 16",value=17,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r20.place(x=550,y=505)

            q6=Label(root,text="6. Which of the following cannot be a variable name in C?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q6.place(x=20,y=560)

            r21=Radiobutton(root,text="true",font="Calibri 16",value=18,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r21.place(x=50,y=605)
            r22=Radiobutton(root,text="volatile",font="Calibri 16",value=2,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r22.place(x=200,y=605)
            r23=Radiobutton(root,text="friend",font="Calibri 16",value=19,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r23.place(x=350,y=605)
            r24=Radiobutton(root,text="export",font="Calibri 16",value=20,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r24.place(x=480,y=605)


            tmin=Entry(root,
                       textvariable=mins,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Arail 50",
                       relief=FLAT,
                       state=DISABLED)

            tmin.place(x=950,y=50)
            
            tsec=Entry(root,
                       textvariable=sec,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Aail 50",
                       relief=FLAT,
                       state=DISABLED)
            tsec.place(x=1050,y=50)
            
            nxt=Button(root,
                       text="Next",
                       relief=GROOVE,
                       width=12,
                       command=win2,
                       font="Calibri 16 bold",
                       bg="#222831",
                       fg="#ffffff"
                       )
            nxt.place(x=930,y=565)
            

        win1()


    def Qcpp():
        java.destroy()
        python.destroy()
        c.destroy()
        cpp.destroy()
        html.destroy()
        css.destroy()
        js.destroy()
        sql.destroy()
        greet.destroy()
        usrl.destroy()
        def win1():
            def win2():
                q1.destroy()
                r1.destroy()
                r2.destroy()
                r3.destroy()
                r4.destroy()
                q2.destroy()
                r5.destroy()
                r6.destroy()
                r7.destroy()
                r8.destroy()
                q3.destroy()
                r8.destroy()
                r9.destroy()
                r10.destroy()
                r11.destroy()
                r12.destroy()
                q4.destroy()
                r13.destroy()
                r14.destroy()
                r15.destroy()
                r16.destroy()
                q5.destroy()
                r17.destroy()
                r18.destroy()
                r19.destroy()
                r20.destroy()
                q6.destroy()
                r21.destroy()
                r22.destroy()
                r23.destroy()
                r24.destroy()
                nxt.destroy()

                def chkans7():
                    r25.config(fg="red")
                    r26.config(fg="red")
                    r27.config(fg="red")
                    r28.config(fg="lightgreen")

                def chkans8():
                    r29.config(fg="red")
                    r30.config(fg="red")
                    r31.config(fg="lightgreen")
                    r32.config(fg="red")

                def chkans9():
                    r33.config(fg="red")
                    r34.config(fg="red")
                    r35.config(fg="lightgreen")
                    r36.config(fg="red")

                def chkans10():
                    r37.config(fg="red")
                    r38.config(fg="lightgreen")
                    r39.config(fg="red")
                    r40.config(fg="red")

                def chkans11():
                    r41.config(fg="red")
                    r42.config(fg="red")
                    r43.config(fg="lightgreen")
                    r44.config(fg="red")

                def chkans12():
                    r45.config(fg="red")
                    r46.config(fg="red")
                    r47.config(fg="lightgreen")
                    r48.config(fg="red")

                
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                q7=Label(root,text="7. Which of the following gives the 4th element of the array?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q7.place(x=20,y=60)

                r25=Radiobutton(root,text="Array[0];",font="Calibri 16",value=21,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r25.place(x=50,y=105)
                r26=Radiobutton(root,text="Array[5];",font="Calibri 16",value=22,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r26.place(x=220,y=105)
                r27=Radiobutton(root,text="Array[4];",font="Calibri 16",value=23,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r27.place(x=400,y=105)
                r28=Radiobutton(root,text="Array[3];",font="Calibri 16",value=2,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r28.place(x=600,y=105)

                q8=Label(root,text="8. Which of the following can be considered as the members that can be inherited but not accessible in any class?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q8.place(x=20,y=160)

                r29=Radiobutton(root,text="Public",font="Calibri 16",value=24,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r29.place(x=50,y=205)
                r30=Radiobutton(root,text="Protected",font="Calibri 16",value=25,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r30.place(x=220,y=205)
                r31=Radiobutton(root,text="Private",font="Calibri 16",value=2,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r31.place(x=400,y=205)
                r32=Radiobutton(root,text="Both A and C",font="Calibri 16",value=26,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r32.place(x=600,y=205)

                q9=Label(root,text="9. Which of the following is not a kind of inheritance?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q9.place(x=20,y=260)

                r33=Radiobutton(root,text="Multiple",font="Calibri 16",value=27,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r33.place(x=50,y=305)
                r34=Radiobutton(root,text="Multi-level",font="Calibri 16",value=28,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r34.place(x=230,y=305)
                r35=Radiobutton(root,text="Distributed",font="Calibri 16",value=2,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r35.place(x=400,y=305)
                r36=Radiobutton(root,text="Hierarchal",font="Calibri 16",value=29,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r36.place(x=600,y=305)

                q10=Label(root,text="10. Which type of approach is used by the C++ language?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q10.place(x=20,y=360)

                r37=Radiobutton(root,text="Right to left",font="Calibri 16",value=30,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r37.place(x=50,y=405)
                r38=Radiobutton(root,text="Left to right",font="Calibri 16",value=2,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r38.place(x=230,y=405)
                r39=Radiobutton(root,text="Top to bottom",font="Calibri 16",value=31,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r39.place(x=380,y=405)
                r40=Radiobutton(root,text="Bottom to up",font="Calibri 16",value=32,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r40.place(x=540,y=405)

                q11=Label(root,text="11. Which one of the following cannot be a friend in C++ languages?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q11.place(x=20,y=460)

                r41=Radiobutton(root,text="A Class",font="Calibri 16",value=33,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r41.place(x=50,y=505)
                r42=Radiobutton(root,text="A Function",font="Calibri 16",value=34,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r42.place(x=200,y=505)
                r43=Radiobutton(root,text="An Object",font="Calibri 16",value=2,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r43.place(x=440,y=505)
                r44=Radiobutton(root,text="None of the above",font="Calibri 16",value=35,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r44.place(x=600,y=505)

                q12=Label(root,text="12. Among the following, which shows the Multiple inheritances?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q12.place(x=20,y=560)

                r45=Radiobutton(root,text="X->Y;X->Z",font="Calibri 16",value=36,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r45.place(x=50,y=605)
                r46=Radiobutton(root,text="X->Y->Z",font="Calibri 16",value=37,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r46.place(x=220,y=605)
                r47=Radiobutton(root,text="X,Y->Z",font="Calibri 16",value=2,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r47.place(x=350,y=605)
                r48=Radiobutton(root,text="None of the above",font="Calibri 16",value=38,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r48.place(x=490,y=605)

                
                def cppBack():
                    h1.destroy()
                    q7.destroy()
                    r25.destroy()
                    r26.destroy()
                    r27.destroy()
                    r28.destroy()
                    q8.destroy()
                    r29.destroy()
                    r30.destroy()
                    r31.destroy()
                    r32.destroy()
                    q9.destroy()
                    r33.destroy()
                    r34.destroy()
                    r35.destroy()
                    r36.destroy()
                    q10.destroy()
                    r37.destroy()
                    r38.destroy()
                    r39.destroy()
                    r40.destroy()
                    q11.destroy()
                    r41.destroy()
                    r42.destroy()
                    r43.destroy()
                    r44.destroy()
                    q12.destroy()
                    r45.destroy()
                    r46.destroy()
                    r47.destroy()
                    r48.destroy()
                    sbtn.destroy()
                    topic1.destroy()
                    tmin.destroy()
                    tsec.destroy()
                    main()

                def froze():
                    sbtn.config(state=DISABLED)
                
                sbtn=Button(root,
                            text="Submit",
                            command=lambda:[ans(),reset()],
                            relief=RAISED,
                            width=25,
                            bd=4,
                            bg="#00fff5",
                            font="Calibri 10",
                            )
                sbtn.place(x=950,y=560)

                topic1=Button(root,
                              text="More quiz",
                              relief=GROOVE,
                              width=25,
                              command=lambda:[cppBack(),reset()],
                              bg="#ffffff"
                              )
                topic1.place(x=950,y=610)

            def chkans1():
                r1.config(fg="red")
                r2.config(fg="lightgreen")
                r3.config(fg="red")
                r4.config(fg="red")

            def chkans2():
                r5.config(fg="red")
                r6.config(fg="red")
                r7.config(fg="lightgreen")
                r8.config(fg="red")

            def chkans3():
                r9.config(fg="red")
                r10.config(fg="red")
                r11.config(fg="red")
                r12.config(fg="lightgreen")

            def chkans4():
                r13.config(fg="red")
                r14.config(fg="lightgreen")
                r15.config(fg="red")
                r16.config(fg="red")

            def chkans5():
                r17.config(fg="red")
                r18.config(fg="red")
                r19.config(fg="lightgreen")
                r20.config(fg="red")

            def chkans6():
                r21.config(fg="red")
                r22.config(fg="lightgreen")
                r23.config(fg="red")
                r24.config(fg="red")

                

            h1 = Label(root,text="--C++ Quiz--",font="Calibri 25 bold",bg="#222831",fg="#ffffff")
            h1.place(x=550,y=0)


            q1=Label(root,text="1. Which of the following is the correct identifier?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q1.place(x=20,y=60)

            r1=Radiobutton(root,text="$var_name",font="Calibri 16",value=1,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r1.place(x=50,y=105)
            r2=Radiobutton(root,text="VAR_123",font="Calibri 16",value=2,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r2.place(x=200,y=105)
            r3=Radiobutton(root,text="varname@",font="Calibri 16",value=3,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r3.place(x=420,y=105)
            r4=Radiobutton(root,text="None of the above",font="Calibri 16",value=4,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r4.place(x=620,y=105)

            q2=Label(root,text="2. Which of the following is the address operator?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q2.place(x=20,y=160)

            r5=Radiobutton(root,text="@",font="Calibri 16",value=5,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r5.place(x=50,y=205)
            r6=Radiobutton(root,text="#",font="Calibri 16",value=6,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r6.place(x=240,y=205)
            r7=Radiobutton(root,text="&",font="Calibri 16",value=2,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r7.place(x=430,y=205)
            r8=Radiobutton(root,text="%",font="Calibri 16",value=8,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r8.place(x=620,y=205)
            
            q3=Label(root,text="3. Which of the following is the original creator of the C++ language?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q3.place(x=20,y=260)

            r9=Radiobutton(root,text="Dennis Ritchie",font="Calibri 16",value=9,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r9.place(x=50,y=305)
            r10=Radiobutton(root,text="Ken Thompson",font="Calibri 16",value=10,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r10.place(x=280,y=305)
            r11=Radiobutton(root,text="Brian Kernighan",font="Calibri 16",value=11,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r11.place(x=510,y=305)
            r12=Radiobutton(root,text="Bjarne Stroustrup",font="Calibri 16",value=2,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r12.place(x=710,y=305)

            q4=Label(root,text="4. Which of the following is the correct syntax to read the single character to console in the C++ language?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q4.place(x=20,y=360)

            r13=Radiobutton(root,text="Read ch()",font="Calibri 16",value=12,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r13.place(x=50,y=405)
            r14=Radiobutton(root,text="get(ch)",font="Calibri 16",value=2,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r14.place(x=230,y=405)
            r15=Radiobutton(root,text="Getline vh()",font="Calibri 16",value=13,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r15.place(x=380,y=405)
            r16=Radiobutton(root,text="Scanf(ch)",font="Calibri 16",value=14,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r16.place(x=530,y=405)

            q5=Label(root,text="5. Which of the following comment syntax is correct to create a single-line comment in the C++ program?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q5.place(x=20,y=460)

            r17=Radiobutton(root,text="/Comment/",font="Calibri 16",value=15,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r17.place(x=50,y=505)
            r18=Radiobutton(root,text="Comment//",font="Calibri 16",value=16,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r18.place(x=200,y=505)
            r19=Radiobutton(root,text="//Comment",font="Calibri 16",value=2,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r19.place(x=390,y=505)
            r20=Radiobutton(root,text="None of the above",font="Calibri 16",value=17,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r20.place(x=550,y=505)

            q6=Label(root,text="6. Which of the following is the correct syntax for declaring the array?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q6.place(x=20,y=560)

            r21=Radiobutton(root,text="init array []",font="Calibri 16",value=18,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r21.place(x=50,y=605)
            r22=Radiobutton(root,text="int array [5];",font="Calibri 16",value=2,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r22.place(x=200,y=605)
            r23=Radiobutton(root,text="Array[5];",font="Calibri 16",value=19,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r23.place(x=350,y=605)
            r24=Radiobutton(root,text="None of the above",font="Calibri 16",value=20,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r24.place(x=480,y=605)

            tmin=Entry(root,
                       textvariable=mins,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Arail 50",
                       relief=FLAT,
                       state=DISABLED)

            tmin.place(x=950,y=50)
            
            tsec=Entry(root,
                       textvariable=sec,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Aail 50",
                       relief=FLAT,
                       state=DISABLED)
            tsec.place(x=1050,y=50)
            
            nxt=Button(root,
                       text="Next",
                       relief=GROOVE,
                       width=12,
                       command=win2,
                       font="Calibri 16 bold",
                       bg="#222831",
                       fg="#ffffff"
                       )
            nxt.place(x=930,y=565)

        win1()

    def Qhtml():
        java.destroy()
        python.destroy()
        c.destroy()
        cpp.destroy()
        html.destroy()
        css.destroy()
        js.destroy()
        sql.destroy()
        greet.destroy()
        usrl.destroy()
        def win1():
            def win2():
                q1.destroy()
                r1.destroy()
                r2.destroy()
                r3.destroy()
                r4.destroy()
                q2.destroy()
                r5.destroy()
                r6.destroy()
                r7.destroy()
                r8.destroy()
                q3.destroy()
                r8.destroy()
                r9.destroy()
                r10.destroy()
                r11.destroy()
                r12.destroy()
                q4.destroy()
                r13.destroy()
                r14.destroy()
                r15.destroy()
                r16.destroy()
                q5.destroy()
                r17.destroy()
                r18.destroy()
                r19.destroy()
                r20.destroy()
                q6.destroy()
                r21.destroy()
                r22.destroy()
                r23.destroy()
                r24.destroy()
                nxt.destroy()

                def chkans7():
                    r25.config(fg="red")
                    r26.config(fg="red")
                    r27.config(fg="red")
                    r28.config(fg="lightgreen")

                def chkans8():
                    r29.config(fg="red")
                    r30.config(fg="red")
                    r31.config(fg="lightgreen")
                    r32.config(fg="red")

                def chkans9():
                    r33.config(fg="red")
                    r34.config(fg="red")
                    r35.config(fg="lightgreen")
                    r36.config(fg="red")

                def chkans10():
                    r37.config(fg="red")
                    r38.config(fg="lightgreen")
                    r39.config(fg="red")
                    r40.config(fg="red")

                def chkans11():
                    r41.config(fg="red")
                    r42.config(fg="red")
                    r43.config(fg="lightgreen")
                    r44.config(fg="red")

                def chkans12():
                    r45.config(fg="red")
                    r46.config(fg="red")
                    r47.config(fg="lightgreen")
                    r48.config(fg="red")
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                q7=Label(root,text="7. Which of the following tag is used to make the underlined text?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q7.place(x=20,y=60)

                r25=Radiobutton(root,text="<pre>",font="Calibri 16",value=21,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r25.place(x=50,y=105)
                r26=Radiobutton(root,text="<ul>",font="Calibri 16",value=22,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r26.place(x=220,y=105)
                r27=Radiobutton(root,text="<underline>",font="Calibri 16",value=23,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r27.place(x=400,y=105)
                r28=Radiobutton(root,text="<u>",font="Calibri 16",value=2,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r28.place(x=600,y=105)

                q8=Label(root,text="8. Which of the following tag is used to define options in a drop-down selection list?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q8.place(x=20,y=160)

                r29=Radiobutton(root,text="<select>",font="Calibri 16",value=24,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r29.place(x=50,y=205)
                r30=Radiobutton(root,text="<list>",font="Calibri 16",value=25,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r30.place(x=220,y=205)
                r31=Radiobutton(root,text="<dropdown>",font="Calibri 16",value=2,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r31.place(x=400,y=205)
                r32=Radiobutton(root,text="<option>",font="Calibri 16",value=26,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r32.place(x=600,y=205)

                q9=Label(root,text="9. HTML tags are enclosed in-",font="Calibri 18",bg="#222831",fg="#ffffff")
                q9.place(x=20,y=270)

                r33=Radiobutton(root,text="# and #",font="Calibri 16",value=27,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r33.place(x=50,y=305)
                r34=Radiobutton(root,text="{ and }",font="Calibri 16",value=28,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r34.place(x=230,y=305)
                r35=Radiobutton(root,text="< and >",font="Calibri 16",value=2,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r35.place(x=400,y=305)
                r36=Radiobutton(root,text="! and ?",font="Calibri 16",value=29,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r36.place(x=600,y=305)

                q10=Label(root,text="10. Which of the following tag is used to add rows in the table?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q10.place(x=20,y=360)

                r37=Radiobutton(root,text="<td> and </td>",font="Calibri 16",value=30,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r37.place(x=50,y=405)
                r38=Radiobutton(root,text="<tr> and </tr>",font="Calibri 16",value=2,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r38.place(x=230,y=405)
                r39=Radiobutton(root,text="<th> and </th>",font="Calibri 16",value=31,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r39.place(x=395,y=405)
                r40=Radiobutton(root,text="None of the above",font="Calibri 16",value=32,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r40.place(x=555,y=405)

                q11=Label(root,text="11. The <hr> tag in HTML is used for -",font="Calibri 18",bg="#222831",fg="#ffffff")
                q11.place(x=20,y=460)

                r41=Radiobutton(root,text="new line",font="Calibri 16",value=33,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r41.place(x=50,y=505)
                r42=Radiobutton(root,text="vertical ruler",font="Calibri 16",value=34,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r42.place(x=200,y=505)
                r43=Radiobutton(root,text="horizontal ruler",font="Calibri 16",value=2,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r43.place(x=410,y=505)
                r44=Radiobutton(root,text="new paragraph",font="Calibri 16",value=35,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r44.place(x=600,y=505)

                q12=Label(root,text="12. Which of the following attribute is used to provide a unique name to an element?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q12.place(x=20,y=560)

                r45=Radiobutton(root,text="class",font="Calibri 16",value=36,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r45.place(x=50,y=605)
                r46=Radiobutton(root,text="type",font="Calibri 16",value=37,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r46.place(x=220,y=605)
                r47=Radiobutton(root,text="id",font="Calibri 16",value=2,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r47.place(x=350,y=605)
                r48=Radiobutton(root,text="None of the above",font="Calibri 16",value=38,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r48.place(x=470,y=605)

                def home():
                    root.destroy()
                    import intro
                
                def htmlBack():
                    h1.destroy()
                    q7.destroy()
                    r25.destroy()
                    r26.destroy()
                    r27.destroy()
                    r28.destroy()
                    q8.destroy()
                    r29.destroy()
                    r30.destroy()
                    r31.destroy()
                    r32.destroy()
                    q9.destroy()
                    r33.destroy()
                    r34.destroy()
                    r35.destroy()
                    r36.destroy()
                    q10.destroy()
                    r37.destroy()
                    r38.destroy()
                    r39.destroy()
                    r40.destroy()
                    q11.destroy()
                    r41.destroy()
                    r42.destroy()
                    r43.destroy()
                    r44.destroy()
                    q12.destroy()
                    r45.destroy()
                    r46.destroy()
                    r47.destroy()
                    r48.destroy()
                    sbtn.destroy()
                    topic1.destroy()
                    tmin.destroy()
                    tsec.destroy()
                    main()

                def froze():
                    sbtn.config(state=DISABLED)

                sbtn=Button(root,
                            text="Submit",
                            command=lambda:[ans(),froze(),reset()],
                            relief=RAISED,
                            width=25,
                            bd=4,
                            bg="#00fff5",
                            font="Calibri 10",
                            )
                sbtn.place(x=950,y=560)

                topic1=Button(root,
                              text="More quiz",
                              relief=GROOVE,
                              width=25,
                              command=lambda:[htmlBack(),reset()],
                              bg="#ffffff"
                              )
                topic1.place(x=950,y=610)

            def chkans1():
                r1.config(fg="red")
                r2.config(fg="lightgreen")
                r3.config(fg="red")
                r4.config(fg="red")

            def chkans2():
                r5.config(fg="red")
                r6.config(fg="red")
                r7.config(fg="lightgreen")
                r8.config(fg="red")

            def chkans3():
                r9.config(fg="red")
                r10.config(fg="red")
                r11.config(fg="red")
                r12.config(fg="lightgreen")

            def chkans4():
                r13.config(fg="red")
                r14.config(fg="lightgreen")
                r15.config(fg="red")
                r16.config(fg="red")

            def chkans5():
                r17.config(fg="red")
                r18.config(fg="red")
                r19.config(fg="lightgreen")
                r20.config(fg="red")

            def chkans6():
                r21.config(fg="red")
                r22.config(fg="lightgreen")
                r23.config(fg="red")
                r24.config(fg="red")

            h1 = Label(root,text="--Html Quiz--",font="Calibri 25 bold",bg="#222831",fg="#ffffff")
            h1.place(x=550,y=0)


            q1=Label(root,text="1. Which of the following element is responsible for making the text bold in HTML?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q1.place(x=20,y=60)

            r1=Radiobutton(root,text="<pre>",font="Calibri 16",value=1,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r1.place(x=50,y=105)
            r2=Radiobutton(root,text="<b>",font="Calibri 16",value=2,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r2.place(x=200,y=105)
            r3=Radiobutton(root,text="<a>",font="Calibri 16",value=3,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r3.place(x=420,y=105)
            r4=Radiobutton(root,text="<br>",font="Calibri 16",value=4,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r4.place(x=620,y=105)

            q2=Label(root,text="2. Which of the following tag is used for inserting the largest heading in HTML?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q2.place(x=20,y=160)

            r5=Radiobutton(root,text="<h2>",font="Calibri 16",value=5,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r5.place(x=50,y=205)
            r6=Radiobutton(root,text="<h3>",font="Calibri 16",value=6,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r6.place(x=240,y=205)
            r7=Radiobutton(root,text="<h1>",font="Calibri 16",value=2,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r7.place(x=430,y=205)
            r8=Radiobutton(root,text="<h6>",font="Calibri 16",value=8,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r8.place(x=620,y=205)
            
            q3=Label(root,text="3. Which of the following tag is used to insert a line-break in HTML?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q3.place(x=20,y=260)

            r9=Radiobutton(root,text="<a>",font="Calibri 16",value=9,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r9.place(x=50,y=305)
            r10=Radiobutton(root,text="<b>",font="Calibri 16",value=10,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r10.place(x=280,y=305)
            r11=Radiobutton(root,text="<break>",font="Calibri 16",value=11,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r11.place(x=510,y=305)
            r12=Radiobutton(root,text="<br>",font="Calibri 16",value=2,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r12.place(x=710,y=305)

            q4=Label(root,text="4. How to create an unordered list (a list with the list items in bullets) in HTML?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q4.place(x=20,y=360)

            r13=Radiobutton(root,text="<ol>",font="Calibri 16",value=12,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r13.place(x=50,y=405)
            r14=Radiobutton(root,text="<ul>",font="Calibri 16",value=2,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r14.place(x=230,y=405)
            r15=Radiobutton(root,text="<li>",font="Calibri 16",value=13,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r15.place(x=380,y=405)
            r16=Radiobutton(root,text="<list>",font="Calibri 16",value=14,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r16.place(x=530,y=405)

            q5=Label(root,text="5. Which character is used to represent the closing of a tag in HTML?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q5.place(x=20,y=460)

            r17=Radiobutton(root,text="<!>",font="Calibri 16",value=15,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r17.place(x=50,y=505)
            r18=Radiobutton(root,text="<\>",font="Calibri 16",value=16,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r18.place(x=200,y=505)
            r19=Radiobutton(root,text="</>",font="Calibri 16",value=2,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r19.place(x=390,y=505)
            r20=Radiobutton(root,text="<#>",font="Calibri 16",value=17,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r20.place(x=550,y=505)

            q6=Label(root,text="6.  Which of the following element is responsible for making the text italic in HTML?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q6.place(x=20,y=560)

            r21=Radiobutton(root,text="<italic>",font="Calibri 16",value=18,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r21.place(x=50,y=605)
            r22=Radiobutton(root,text="<i>",font="Calibri 16",value=2,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r22.place(x=200,y=605)
            r23=Radiobutton(root,text="<it>",font="Calibri 16",value=19,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r23.place(x=350,y=605)
            r24=Radiobutton(root,text="<pre>",font="Calibri 16",value=20,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r24.place(x=480,y=605)

            tmin=Entry(root,
                       textvariable=mins,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Arail 50",
                       relief=FLAT,
                       state=DISABLED)

            tmin.place(x=950,y=50)
            
            tsec=Entry(root,
                       textvariable=sec,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Aail 50",
                       relief=FLAT,
                       state=DISABLED)
            tsec.place(x=1050,y=50)
            
            nxt=Button(root,
                       text="Next",
                       relief=GROOVE,
                       width=12,
                       command=win2,
                       font="Calibri 16 bold",
                       bg="#222831",
                       fg="#ffffff"
                       )
            nxt.place(x=930,y=565)

        win1()

    def Qcss():
        java.destroy()
        python.destroy()
        c.destroy()
        cpp.destroy()
        html.destroy()
        css.destroy()
        js.destroy()
        sql.destroy()
        greet.destroy()
        usrl.destroy()
        def win1():
            def win2():
                q1.destroy()
                r1.destroy()
                r2.destroy()
                r3.destroy()
                r4.destroy()
                q2.destroy()
                r5.destroy()
                r6.destroy()
                r7.destroy()
                r8.destroy()
                q3.destroy()
                r8.destroy()
                r9.destroy()
                r10.destroy()
                r11.destroy()
                r12.destroy()
                q4.destroy()
                r13.destroy()
                r14.destroy()
                r15.destroy()
                r16.destroy()
                q5.destroy()
                r17.destroy()
                r18.destroy()
                r19.destroy()
                r20.destroy()
                q6.destroy()
                r21.destroy()
                r22.destroy()
                r23.destroy()
                r24.destroy()
                nxt.destroy()

                def chkans7():
                    r25.config(fg="red")
                    r26.config(fg="red")
                    r27.config(fg="red")
                    r28.config(fg="lightgreen")

                def chkans8():
                    r29.config(fg="red")
                    r30.config(fg="red")
                    r31.config(fg="lightgreen")
                    r32.config(fg="red")

                def chkans9():
                    r33.config(fg="red")
                    r34.config(fg="red")
                    r35.config(fg="lightgreen")
                    r36.config(fg="red")

                def chkans10():
                    r37.config(fg="red")
                    r38.config(fg="lightgreen")
                    r39.config(fg="red")
                    r40.config(fg="red")

                def chkans11():
                    r41.config(fg="red")
                    r42.config(fg="red")
                    r43.config(fg="lightgreen")
                    r44.config(fg="red")

                def chkans12():
                    r45.config(fg="red")
                    r46.config(fg="red")
                    r47.config(fg="lightgreen")
                    r48.config(fg="red")
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                q7=Label(root,text="7. Which of the following is the correct syntax to select all paragraph elements in a div element?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q7.place(x=20,y=60)

                r25=Radiobutton(root,text="p",font="Calibri 16",value=21,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r25.place(x=50,y=105)
                r26=Radiobutton(root,text="div ~ p",font="Calibri 16",value=22,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r26.place(x=220,y=105)
                r27=Radiobutton(root,text="div#p",font="Calibri 16",value=23,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r27.place(x=400,y=105)
                r28=Radiobutton(root,text="p",font="Calibri 16",value=2,variable=var6,command=chkans7,bg="#222831",fg="#ffffff",activebackground="#222831")
                r28.place(x=600,y=105)

                q8=Label(root,text="8. Which of the following is not a type of combinator?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q8.place(x=20,y=160)

                r29=Radiobutton(root,text=">",font="Calibri 16",value=24,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r29.place(x=50,y=205)
                r30=Radiobutton(root,text="+",font="Calibri 16",value=25,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r30.place(x=220,y=205)
                r31=Radiobutton(root,text="*",font="Calibri 16",value=2,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r31.place(x=400,y=205)
                r32=Radiobutton(root,text="~",font="Calibri 16",value=26,variable=var7,command=chkans8,bg="#222831",fg="#ffffff",activebackground="#222831")
                r32.place(x=600,y=205)

                q9=Label(root,text="9. Which of the following defines 1% of viewport height?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q9.place(x=20,y=260)

                r33=Radiobutton(root,text="px",font="Arail 15",value=27,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r33.place(x=50,y=305)
                r34=Radiobutton(root,text="vw",font="Arail 15",value=28,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r34.place(x=230,y=305)
                r35=Radiobutton(root,text="vh",font="Arail 15",value=2,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r35.place(x=400,y=305)
                r36=Radiobutton(root,text="vmin",font="Arail 15",value=29,variable=var8,command=chkans9,bg="#222831",fg="#ffffff",activebackground="#222831")
                r36.place(x=600,y=305)

                q10=Label(root,text="10. Which of the following is not the value for an unordered list?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q10.place(x=20,y=360)

                r37=Radiobutton(root,text="circle",font="Calibri 16",value=30,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r37.place(x=50,y=405)
                r38=Radiobutton(root,text="numeric",font="Calibri 16",value=2,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r38.place(x=230,y=405)
                r39=Radiobutton(root,text="square",font="Calibri 16",value=31,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r39.place(x=395,y=405)
                r40=Radiobutton(root,text="disc",font="Calibri 16",value=32,variable=var9,command=chkans10,bg="#222831",fg="#ffffff",activebackground="#222831")
                r40.place(x=555,y=405)

                q11=Label(root,text="11. Which is not a box-level element?",font="Calibri 18",bg="#222831",fg="#ffffff")
                q11.place(x=20,y=460)

                r41=Radiobutton(root,text="<p>",font="Calibri 16",value=33,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r41.place(x=50,y=505)
                r42=Radiobutton(root,text="<ul>",font="Calibri 16",value=34,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r42.place(x=200,y=505)
                r43=Radiobutton(root,text="<b>",font="Calibri 16",value=2,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r43.place(x=410,y=505)
                r44=Radiobutton(root,text="<li>",font="Calibri 16",value=35,variable=var10,command=chkans11,bg="#222831",fg="#ffffff",activebackground="#222831")
                r44.place(x=600,y=505)

                q12=Label(root,text="12. Which of the following will move the image up and down??",font="Calibri 18",bg="#222831",fg="#ffffff")
                q12.place(x=20,y=560)

                r45=Radiobutton(root,text="fixed",font="Calibri 16",value=36,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r45.place(x=50,y=605)
                r46=Radiobutton(root,text="repeat-x",font="Calibri 16",value=37,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r46.place(x=220,y=605)
                r47=Radiobutton(root,text="scroll",font="Calibri 16",value=2,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r47.place(x=350,y=605)
                r48=Radiobutton(root,text="repeat-y",font="Calibri 16",value=38,variable=var11,command=chkans12,bg="#222831",fg="#ffffff",activebackground="#222831")
                r48.place(x=470,y=605)

                def home():
                    root.destroy()
                    import intro
                
                def cssBack():
                    h1.destroy()
                    q7.destroy()
                    r25.destroy()
                    r26.destroy()
                    r27.destroy()
                    r28.destroy()
                    q8.destroy()
                    r29.destroy()
                    r30.destroy()
                    r31.destroy()
                    r32.destroy()
                    q9.destroy()
                    r33.destroy()
                    r34.destroy()
                    r35.destroy()
                    r36.destroy()
                    q10.destroy()
                    r37.destroy()
                    r38.destroy()
                    r39.destroy()
                    r40.destroy()
                    q11.destroy()
                    r41.destroy()
                    r42.destroy()
                    r43.destroy()
                    r44.destroy()
                    q12.destroy()
                    r45.destroy()
                    r46.destroy()
                    r47.destroy()
                    r48.destroy()
                    sbtn.destroy()
                    topic1.destroy()
                    tmin.destroy()
                    tsec.destroy()
                    main()

                def froze():
                    sbtn.config(state=DISABLED)

                sbtn=Button(root,
                            text="Submit",
                            command=lambda:[ans(),froze(),reset()],
                            relief=RAISED,
                            width=25,
                            bd=4,
                            bg="#00fff5",
                            font="Calibri 10",
                            )
                sbtn.place(x=950,y=560)


                topic1=Button(root,
                              text="More quiz",
                              relief=GROOVE,
                              width=25,
                              command=lambda:[cssBack(),reset()],
                              bg="#ffffff"
                              )
                topic1.place(x=950,y=610)

            def chkans1():
                r1.config(fg="red")
                r2.config(fg="lightgreen")
                r3.config(fg="red")
                r4.config(fg="red")

            def chkans2():
                r5.config(fg="red")
                r6.config(fg="red")
                r7.config(fg="lightgreen")
                r8.config(fg="red")

            def chkans3():
                r9.config(fg="red")
                r10.config(fg="red")
                r11.config(fg="red")
                r12.config(fg="lightgreen")

            def chkans4():
                r13.config(fg="red")
                r14.config(fg="lightgreen")
                r15.config(fg="red")
                r16.config(fg="red")

            def chkans5():
                r17.config(fg="red")
                r18.config(fg="red")
                r19.config(fg="lightgreen")
                r20.config(fg="red")

            def chkans6():
                r21.config(fg="red")
                r22.config(fg="lightgreen")
                r23.config(fg="red")
                r24.config(fg="red")

            h1 = Label(root,text="--Css3 Quiz--",font="Calibri 25 bold",bg="#222831",fg="#ffffff")
            h1.place(x=550,y=0)


            q1=Label(root,text="1. The property in CSS used to change the background color of an element is -",font="Calibri 18",bg="#222831",fg="#ffffff")
            q1.place(x=20,y=60)

            r1=Radiobutton(root,text="bgcolor",font="Calibri 16",value=1,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r1.place(x=50,y=105)
            r2=Radiobutton(root,text="background-color",font="Calibri 16",value=2,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r2.place(x=200,y=105)
            r3=Radiobutton(root,text="color",font="Calibri 16",value=3,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r3.place(x=420,y=105)
            r4=Radiobutton(root,text="All of the above",font="Calibri 16",value=4,variable=var,command=chkans1,bg="#222831",fg="#ffffff",activebackground="#222831")
            r4.place(x=620,y=105)

            q2=Label(root,text="2. The property in CSS used to change the text color of an element is -",font="Calibri 18",bg="#222831",fg="#ffffff")
            q2.place(x=20,y=160)

            r5=Radiobutton(root,text="colour",font="Calibri 16",value=5,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r5.place(x=50,y=205)
            r6=Radiobutton(root,text="bgcolor",font="Calibri 16",value=6,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r6.place(x=240,y=205)
            r7=Radiobutton(root,text="color",font="Calibri 16",value=2,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r7.place(x=430,y=205)
            r8=Radiobutton(root,text="background-color",font="Calibri 16",value=8,variable=var1,command=chkans2,bg="#222831",fg="#ffffff",activebackground="#222831")
            r8.place(x=620,y=205)
            
            q3=Label(root,text="3. The HTML attribute used to define the inline styles is -",font="Calibri 18",bg="#222831",fg="#ffffff")
            q3.place(x=20,y=260)

            r9=Radiobutton(root,text="id",font="Calibri 16",value=9,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r9.place(x=50,y=305)
            r10=Radiobutton(root,text="class",font="Calibri 16",value=10,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r10.place(x=280,y=305)
            r11=Radiobutton(root,text="styles",font="Calibri 16",value=11,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r11.place(x=510,y=305)
            r12=Radiobutton(root,text="style",font="Calibri 16",value=2,variable=var2,command=chkans3,bg="#222831",fg="#ffffff",activebackground="#222831")
            r12.place(x=710,y=305)

            q4=Label(root,text="4. The HTML attribute used to define the internal stylesheet is -",font="Calibri 18",bg="#222831",fg="#ffffff")
            q4.place(x=20,y=360)

            r13=Radiobutton(root,text="<link>",font="Calibri 16",value=12,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r13.place(x=50,y=405)
            r14=Radiobutton(root,text="<style>",font="Calibri 16",value=2,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r14.place(x=230,y=405)
            r15=Radiobutton(root,text="<script>",font="Calibri 16",value=13,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r15.place(x=380,y=405)
            r16=Radiobutton(root,text="<stylesheet>",font="Calibri 16",value=14,variable=var3,command=chkans4,bg="#222831",fg="#ffffff",activebackground="#222831")
            r16.place(x=530,y=405)

            q5=Label(root,text="5. Are the negative values allowed in padding property?",font="Calibri 18",bg="#222831",fg="#ffffff")
            q5.place(x=20,y=460)

            r17=Radiobutton(root,text="Can't say",font="Calibri 16",value=15,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r17.place(x=50,y=505)
            r18=Radiobutton(root,text="Yes",font="Calibri 16",value=16,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r18.place(x=200,y=505)
            r19=Radiobutton(root,text="No",font="Calibri 16",value=2,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r19.place(x=390,y=505)
            r20=Radiobutton(root,text="May be",font="Calibri 16",value=17,variable=var4,command=chkans5,bg="#222831",fg="#ffffff",activebackground="#222831")
            r20.place(x=550,y=505)

            q6=Label(root,text="6.  The CSS property used to specify the transparency of an element is -",font="Calibri 18",bg="#222831",fg="#ffffff")
            q6.place(x=20,y=560)

            r21=Radiobutton(root,text="filter",font="Calibri 16",value=18,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r21.place(x=50,y=605)
            r22=Radiobutton(root,text="opacity",font="Calibri 16",value=2,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r22.place(x=200,y=605)
            r23=Radiobutton(root,text="visibility",font="Calibri 16",value=19,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r23.place(x=350,y=605)
            r24=Radiobutton(root,text="overlay",font="Calibri 16",value=20,variable=var5,command=chkans6,bg="#222831",fg="#ffffff",activebackground="#222831")
            r24.place(x=480,y=605)

            tmin=Entry(root,
                       textvariable=mins,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Arail 50",
                       relief=FLAT,
                       state=DISABLED)

            tmin.place(x=980,y=50)
            
            tsec=Entry(root,
                       textvariable=sec,
                       width=2,
                       fg="red",
                       bg="#f6f6f6",
                       justify=CENTER,
                       font="Aail 50",
                       relief=FLAT,
                       state=DISABLED)
            tsec.place(x=1080,y=50)
            
            nxt=Button(root,
                       text="Next",
                       relief=GROOVE,
                       width=12,
                       command=win2,
                       font="Calibri 16 bold",
                       bg="#222831",
                       fg="#ffffff"
                       )
            nxt.place(x=930,y=565)

        win1()
    
    java = Button(root,
                  text="Java",
                  image=javapic,
                  height=120,
                  width=120,
                  relief=RAISED,
                  bd=4,
                  command=lambda:[Qjava(),countdown()],
                  bg="White")

    java.place(x=130,y=120)


    python = Button(root,
                    image=pythonpic,
                    height=120,
                    width=120,
                    relief=RAISED,
                    bd=4,
                    bg="white",
                    command=lambda:[Qpython(),countdown()])

    python.place(x=420,y=120)

    c = Button(root,
               image=cpic,
               height=120,
               width=120,
               relief=RAISED,
               bd=4,
               bg="white",
               command=lambda:[Qc(),countdown()])

    c.place(x=720,y=120)

    cpp = Button(root,
                 image=cpppic,
                 height=120,
                 width=120,
                 relief=RAISED,
                 bd=4,
                 bg="white",
                 command=lambda:[Qcpp(),countdown()])

    cpp.place(x=1000,y=120)

    html = Button(root,
                  image=htmlpic,
                  height=120,
                  width=120,
                  relief=RAISED,
                  bd=4,
                  bg="white",
                  command=lambda:[Qhtml(),countdown()])

    html.place(x=130,y=420)

    css = Button(root,
                 image=css3pic,
                 height=120,
                 width=120,
                 relief=RAISED,
                 bd=4,
                 bg="white",
                 command=lambda:[Qcss(),countdown()])

    css.place(x=420,y=420)

    js = Button(root,
                image=jspic,
                height=120,
                width=120,
                relief=RAISED,
                bd=4,
                bg="white",
                command=Qjs)

    js.place(x=720,y=420)

    sql = Button(root,
                 image=sqlpic,
                 height=120,
                 width=120,
                 relief=RAISED,
                 bd=4,
                 bg="white",
                 command=Qsql)

    sql.place(x=1000,y=420)


    hour = datetime.datetime.now().hour
    gm=11
    ga=16
    if (hour <= gm):
        greet=Label(root,text="Good Morning,",font="Calibri 22 bold",bg="#222831",fg="#ffffff")
        greet.place(x=10,y=10)
    elif(hour <= ga):
        greet=Label(root,text="Good Afternoon,",font="Calibri 22 bold",bg="#222831",fg="#ffffff")
        greet.place(x=10,y=10)
    else:
        greet=Label(root,text="Good Evening,",font="Calibri 22 bold",bg="#222831",fg="#ffffff")
        greet.place(x=10,y=10)

    fil = open("10111.txt","r")
    filr = fil.readlines()
    for i in filr:
        pass   

    usrl = Label(root,text=i,font="Calibri 22 bold",bg="#222831",fg="#ffffff")
    usrl.place(x=220,y=10)
    fil.close()

main()
root.mainloop()
