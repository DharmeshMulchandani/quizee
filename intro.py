from tkinter import *
from tkinter import messagebox
import os
import re
root = Tk()

root.state('zoomed')
root.title('--Quizee--')
root.config(bg="#222831")

def win():
    var1 = IntVar()
    def registration():
        welcome.destroy()
        logo.destroy()
        reg.destroy()
        log.destroy()
        rule1.destroy()
        rule2.destroy()
        rule3.destroy()
        sn.destroy()

        TN = Label(root,text="-- Register --",font=("Calibri 25"),bg="#00adb5",fg="#ffffff")
        TN.place(x=550,y=0)

        ins = Label(root,text = '"Testing leads to failure, and failure leads to understanding."',font=("Calibri italic",18),fg="#ffffff",bg="#222831")
        ins.place(x=360,y=100)

        EI = Label(root,text = "Email Id : ",font=("Calibri",20),bg="#222831",fg="#ffffff")
        EI.place(x=350,y=160)
        e4 = Entry(root,bd="3",insertbackground="#ffffff",width="55",bg="#222831",fg="#ffffff",relief=GROOVE,font="Calibri 15")
        e4.place(x=350,y=200)

        UN = Label(root,text = "User Name : ",font=("Calibri",20),bg="#222831",fg="#ffffff")
        UN.place(x=350,y=270)
        e5 = Entry(root,bd="3",insertbackground="#ffffff",width="25",bg="#222831",fg="#ffffff",relief=GROOVE,font="Calibri 15")
        e5.place(x=350,y=310)

        PASS = Label(root,text = "Password : ",font=("Calibri",20),bg="#222831",fg="#ffffff")
        PASS.place(x=630,y=270)
        e3 = Entry(root,bd="3",show="*",insertbackground="#ffffff",width="25",bg="#222831",fg="#ffffff",relief=GROOVE,font="Calibri 15")
        e3.place(x=630,y=310)

        chk1 = Checkbutton(root,bg="#222831",activebackground="#222831",width=10,variable=var1)
        chk1.place(x=520,y=370)

        con = Label(root,text="I Agree with all condition",bg="#222831",fg="#ffffff")
        con.place(x=586,y=372)

        
        
        def check_entry():
            chkp=e3.get()
            chkemail=e4.get()
            chkun=e5.get()
            chka=var1.get()

            d=len(chkp)
            regex = re.compile(r"([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|\[[\t -Z^-~]*])")

            if (chkp and chkemail and chkun and chka) == True and d>=8 and re.fullmatch(regex, chkemail):
                messagebox.showinfo("Quizee","Sucessful")
                intro()
            elif d<8:
                messagebox.showerror("Quizee","Passworld Length Should be Greater then 7")
            else:
                messagebox.showerror("Quizee","Enter all the fields Properly")
           
        def insert():
            p=e3.get()
            email=e4.get()
            un=e5.get()

            pdata=[]
            undata=[]

            pdata.append(p)
            undata.append(un)
            
            udm=str(undata)
            pdm=str(pdata)
            f1 = open("data.txt","a") 
            f1.write(udm)
            f1.write("\n")
            f1.write(pdm)
            f1.close()
            
        sbtn = Button(root,
                      text="Submit",
                      width="25",
                      bg="#00fff5",
                      bd="1",
                      font=("Calibri",15),
                      command=lambda:[insert(),check_entry()],
                      relief=GROOVE
                      )
        sbtn.place(x=510,y=415)
        
        def intro():
            TN.destroy()
            PASS.destroy()
            e3.destroy()
            EI.destroy()
            e4.destroy()
            UN.destroy()
            e5.destroy()
            sbtn.destroy()
            cbtn.destroy()
            ins.destroy()
            chk1.destroy()
            con.destroy()
            win()
        cbtn = Button(root,
                      text="<-- Back",
                      command=intro,
                      width="15",
                      bg="white",
                      bd="1",
                      font=("Calibri",12),
                      relief=GROOVE
                      )
        cbtn.place(x=25,y=560)
    def login():
        welcome.destroy()
        logo.destroy()
        reg.destroy()
        log.destroy()
        rule1.destroy()
        rule2.destroy()
        rule3.destroy()
        sn.destroy()
        
        lh=Label(root,text="--Login--",font=("Calibri",35),bg="#222831",fg="#ffffff")
        lh.place(x=565,y=0)

        lun = Label(root,text="Username: ",font=("Calibri",20),bg="#222831",fg="#ffffff")
        lun.place(x=530,y=160)
        le1 = Entry(root,bd="2",insertbackground="#ffffff",width="25",bg="#222831",fg="#ffffff",relief=GROOVE,font="Calibri 15")
        le1.place(x=530,y=210)

        lpass = Label(root,text = "Password : ",font=("Calibri",20),bg="#222831",fg="#ffffff")
        lpass.place(x=530,y=260)
        le2 = Entry(root,bd="2",show="*",insertbackground="#ffffff",width="25",bg="#222831",fg="#ffffff",relief=GROOVE,font="Calibri 15")
        le2.place(x=530,y=310)

        

        def intro1():
            lh.destroy()
            lun.destroy()
            le1.destroy()
            lpass.destroy()
            le2.destroy()
            startbtn.destroy()
            homebtn.destroy()
            win()
        homebtn = Button(root,
                         text="<-- Back",
                         command=intro1,
                         width="15",
                         bg="#ffffff",
                         bd="1",
                         font=("Calibri",12),
                         relief=GROOVE)
        homebtn.place(x=25,y=560)
        
        def check():
            clun=le1.get()
            clpass=le2.get()

            f3 = open("10111.txt","a")
            f3.write(clun +'\n')
            f3.close()

            f2 = open("data.txt","r+")
            readf1 = f2.read()
            if clun in readf1 and clpass in readf1:
                messagebox.showinfo("Welome",clun)
                root.destroy()
                import topic
            else:
                messagebox.showerror("Error","Inavalid")
            f2.close()

            


        startbtn = Button(root,
                      text="Start",
                      command=check,
                      width="25",
                      bg="#00fff5",
                      bd="1",
                      font=("Calibri",15)
                      )
        startbtn.place(x=530,y=360)

    welcome = Label(root,text="Welcome To",font="Fixedsys 30",bg="#222831",fg="#ffffff")
    welcome.place(x=550,y=100)

    logo = Label(root,text="Quizee",font="Fixedsys 70 bold",bg="#222831",fg="#ffffff")
    logo.place(x=500,y=170)

    sn = Label(root,text="Created by Dharmesh..",font="Arail 15 bold",fg="lightgreen",bg="#222831")
    sn.place(x=540,y=390)

    rule1 = Label(root,text="-- If you are New User first register",font="Arail 15",fg="red",bg="#222831")
    rule1.place(x=50,y=470)
    
    rule2 = Label(root,text="-- Ones you going on next page of any quiz you cant go to previous page",font="Arail 15",fg="red",bg="#222831")
    rule2.place(x=50,y=510)

    rule3 = Label(root,text="-- If time is up quiz will be automatically sumbmitted",font="Arail 15",fg="red",bg="#222831")
    rule3.place(x=50,y=550)
    

    

    reg=Button(root,
               text="New user",
               relief=GROOVE,
               bd=2,
               fg="#ffffff",
               bg="#222831",
               width=15,
               command=registration,
               font="Calibri"
               )
    reg.place(x=470,y=320)

    log=Button(root,
               text="Login",
               relief=SOLID,
               bd=4,
               width=15,
               command=login,
               font="Calibri"
               )
    log.place(x=680,y=320)

win()

root.mainloop()
